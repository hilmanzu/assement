import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import login from './menu/auth/login.js'

const App = () => (
  <BrowserRouter>
      <Switch>
        <Route exact path="/register" component={login} />
        <Route path="/" component={login} />
      </Switch>
  </BrowserRouter>
)

export default App;

var config = {
    apiKey: "AIzaSyASWOqUppmRp0XFVb8uw8RTHbiVnJ6E_xY",
    authDomain: "asesmen-hr.firebaseapp.com",
    databaseURL: "https://asesmen-hr.firebaseio.com",
    projectId: "asesmen-hr",
    storageBucket: "asesmen-hr.appspot.com",
    messagingSenderId: "661722095498"
  };
firebase.initializeApp(config);
